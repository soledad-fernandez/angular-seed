'use strict';

var application = angular.module('ngSeed', [
		// dependencies here
	]);

application.controller('mainCtrl', ['$scope', '$interval', function($scope , $interval){
		
		var svg = d3.select("svg");
		var total = 0;

		$scope.nums = [];

        $scope.$watch('nums', function(){
        	// console.log($scope.nums);
        	updateCircles();
        }, true);

        $interval(function(){
        	console.log("add circle");
        	$scope.addCircle();
        }, 50);

        function updateCircles() {

        	var circle = svg.selectAll("circle")
                .data($scope.nums, function(d){return d.ind;});

           	circle
           		.attr("r", function(d){return Math.sqrt(d.val)})
           		.attr("cx", function(d, i){
           			if(Math.random() > 0.5) {
           				return parseFloat(this.attributes.cx.value) - Math.random() * 8;	
           			} else {
           				return parseFloat(this.attributes.cx.value) + Math.random() * 5;	
           			}
           			
           		})
           		.attr("cy", function(d, i){
           			if(Math.random() > 0.5) {
           				return parseFloat(this.attributes.cy.value) - Math.random() * 8;	
           			} else {
           				return parseFloat(this.attributes.cy.value) + Math.random() * 5;	
           			}
           		})
           		.text(function(d){return d.val;});

	        circle.enter().append("circle")
	            .attr("fill", "steelblue")
	            .attr("r", function(d){return Math.sqrt(d.val)})
	            .attr("cx", function(d, i){return Math.floor(Math.random() * 1000)})
	            .attr("cy", function(d, i){return Math.floor(Math.random() * 500)})
	            .text(function(d){return d.val;});

	        circle.exit().remove();
        }

		$scope.addCircle = function(){
			var n = $scope.nums;
			n = n.map(function(d){
					d.val = Math.floor(d.val/ (1.01));
					return d;
				});
			n = n.filter(function(n){
					return n.val>0;
				});
			n.push({ind: total++, val: Math.floor(Math.random() * 1000)});
			console.log(n.length);
			$scope.nums = n;
		}

		$scope.removeCircle = function(){
			$scope.nums.pop();
			// console.log($scope.nums);
		}

	}


]); 